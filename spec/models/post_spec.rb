require 'rails_helper'

RSpec.describe Post, type: :model do
  context 'test validation of post model' do
    it 'ensure that title is present' do
      post = Post.new(body: "loren ipsum").save
      expect(post).to eq(false)
    end

    it 'ensure that body is present' do
      post = Post.new(title: "loren ipsum").save
      expect(post).to eq(false)
    end

    it 'Should save sucessfully' do
      post = Post.new(body: "loren ipsum", title: "title").save
      expect(post).to eq(true)
    end
  end

  context 'scope test' do
    let (:params) {{title: 'my title', body: 'my body'}}
    before(:each) do
      Post.new(params).save
      Post.new(params).save
      Post.new(params).save
      Post.new(params.merge(is_deleted: true)).save
      Post.new(params.merge(is_deleted: true)).save
    end

    it 'should return all the active post' do
      expect(Post.not_soft_destroyed.size).to eq(3)
    end

    it 'should return all the soft deleted post' do
      expect(Post.soft_destroyed.size).to eq(2)
    end

    it 'should return all the post' do
      expect(Post.with_soft_destroyed.size).to eq(5)
    end
  end
end
