require 'rails_helper'

RSpec.describe Comment, type: :model do
  context 'test validation of comment model' do
    it 'ensure that user name is present' do
      comment = Comment.new(body: "loren ipsum", post_id: 2).save
      expect(comment).to eq(false)
    end

    it 'ensure that body is present' do
      comment = Comment.new(user_name: "loren ipsum", post_id: 2).save
      expect(comment).to eq(false)
    end

    it 'ensure that post_id is present' do
      comment = Comment.new(user_name: "loren ipsum", body: "loren ipsum").save
      expect(comment).to eq(false)
    end

    it 'Should save sucessfully' do
      comment = Comment.new(body: "loren ipsum", user_name: "title", post_id: 2).save
      expect(comment).to eq(true)
    end

    it 'should belongs to post' do
      t = Comment.reflect_on_association(:post)
      expect(t.macro).to eq(:belongs_to)
    end
  end

  context 'scope test' do
    let (:params) {{user_name: 'my title', body: 'my body', post_id: 2}}
    before(:each) do
      Comment.new(params).save
      Comment.new(params).save
      Comment.new(params).save
      Comment.new(params.merge(is_deleted: true)).save
      Comment.new(params.merge(is_deleted: true)).save
    end

    it 'should return all the active comment' do
      expect(Comment.not_soft_destroyed.size).to eq(3)
    end

    it 'should return all the soft deleted comment' do
      expect(Comment.soft_destroyed.size).to eq(2)
    end

    it 'should return all the comment' do
      expect(Comment.with_soft_destroyed.size).to eq(5)
    end
  end
end
