Rails.application.routes.draw do
  root "posts#index"

  resources :posts do
    resources :comments
    post '/soft_destroy' => 'comments#soft_destroy', :as => :comment_soft_destroy
  end
  post '/soft_destroy' => 'posts#soft_destroy', :as => :soft_destroy

  post '/restore' => 'trashes#restore' ,:as => :restore
  get '/trash' => 'trashes#index', :as => :trash
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
