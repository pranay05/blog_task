json.extract! comment, :id, :post_id, :body, :is_deleted, :created_at, :updated_at
json.url comment_url(comment, format: :json)
