json.extract! post, :id, :title, :body, :is_deleted, :created_at, :updated_at
json.url post_url(post, format: :json)
