class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy, :soft_destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.not_soft_destroyed
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    respond_to do |format|
        format.html
        format.js   { render 'show.js.erb' }
    end
  rescue Exception => e
    redirect_to root_path, alert: e.message
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  rescue Exception => e
    redirect_to root_path, alert: e.message
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    respond_to do |format|
      if @post.save
        format.html
        format.js   { render 'create.js.erb' }
      else
        @error = @post.errors.messages
        format.html { render :action => 'new' }
        format.js   { render 'fail_create.js.erb' }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html
        format.js   { render 'update.js.erb' }
      else
        @error = @post.errors.messages
        format.html { render :action => 'edit' }
        format.js   { render 'posts/failed_update.js.erb' }
      end
    end

  rescue Exception => e
    redirect_to root_path, alert: e.message
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    begin
    @post_id = @post.id
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.js   { render 'posts/destroy.js.erb' }
    end
    rescue Exception => e
      redirect_to root_path, alert: e.message
    end
  end

  def soft_destroy
    begin
      @post_id = @post.id
      @post.soft_destroy
      respond_to do |format|
        format.html
        format.js   { render 'posts/soft_destroy.js.erb' }
      end

  rescue Exception => e
    redirect_to root_path, alert: e.message
  end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
      Exception.new("poast with id is not found") if @post.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :body, :is_deleted)
    end
end
