class CommentsController < ApplicationController
  def create
    begin
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create!(params[:comment].permit(:user_name, :body, :post_id))
    respond_to do |format|
      format.html
      format.js   { render 'create.js.erb' }
    end
    rescue Exception => e
      redirect_to root_path, alert: e.message
    end
  end


  def edit
    begin
    @comment = Comment.find(params[:id])
    @post = Post.find(params[:post_id])
    rescue Exception => e
      redirect_to root_path, alert: e.message
    end
  end

  def update
    begin
    @comment = Comment.find(params[:id])
    @comment.update!(params[:comment].permit(:user_name, :body, :post_id))
    respond_to do |format|
      format.html
      format.js   { render 'update.js.erb' }
    end
    rescue Exception => e
      redirect_to root_path, alert: e.message
      end
  end

  def destroy
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html
      format.js   { render 'destroy.js.erb' }
    end
  end

  def soft_destroy
    begin
      @post = Post.find(params[:post_id])
      @comment = @post.comments.find(params[:id])
      @comment.soft_destroy
      respond_to do |format|
        format.html
        format.js   { render 'comments/soft_destroy.js.erb' }
      end

    rescue Exception => e
      redirect_to root_path, alert: e.message
    end
  end
end
