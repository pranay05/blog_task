class TrashesController < ApplicationController
  def index
    begin
    @posts = Post.soft_destroyed
    @deleted_comments = Post.get_all_soft_deleted_comments
    @map_comments_according_to_post = {}
    @deleted_comments.each do |post|
      comment = []
      post.comments.each do |com|
        comment << com
      end
      @map_comments_according_to_post[post.id] = comment
    end
    rescue Exception => e
      redirect_to trash_path, alert: e.message
    end
  end

  def restore
    begin
      if params[:post_id].present? && params[:id].present?
        @comment = Comment.find(params[:id])
        @comment.recover
      elsif params[:post_id].present?
        @post = Post.find(params[:post_id])
        @post.recover
      end
  rescue Exception => e
    redirect_to trash_path, alert: e.message
    end
  end

end

