class Comment < ApplicationRecord
  belongs_to :post, optional: true
  validates :post_id, presence: true
  validates :user_name, presence: true, length: {minimum: 2}
  validates :body,  presence: true
  include SoftDeletable

end
