module SoftDeletable
  extend ActiveSupport::Concern

  included do
    scope :soft_destroyed, ->{ where(is_deleted: true) }
    scope :not_soft_destroyed, ->{ where(is_deleted: false) }
    scope :with_soft_destroyed, ->{ unscope(where: :is_deleted)}

    default_scope { with_soft_destroyed }
  end

  def soft_destroy
    update_column :is_deleted, true
  end

  def recover
    update_column :is_deleted, false
  end
end