class Post < ApplicationRecord

  has_many :comments, dependent: :destroy
  validates :title, presence: true, length: {minimum: 5}
  validates :body,  presence: true
  include SoftDeletable

  scope :get_all_soft_deleted_comments, ->{ joins(:comments).where("comments.is_deleted =?",true).uniq }

end
