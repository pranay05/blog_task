Ruby version - 2.4.1

Rails version - rails, 5.1.6

psql (PostgreSQL) 10.5

How to run this project 

1) clone project git clone git clone https://pranay05@bitbucket.org/pranay05/blog_task.git

2) fetch all gem libraries bundle install

3) create database rake db:create

4) migrate database rake db:migrate

5) run server rails s

6) rspec - for running all the test cases

Summary:

Assumptions and notes:

There is scope for authetication/authorization, but for simplicity of project we dont have any user authetication/authorization right now.

This project is basically divided into 2 modules.

 1) Blog page - containing all the posts and comments

2) Trash page - containing all the soft deleted post and comments.

Here is the link : https://polar-reef-32509.herokuapp.com/
